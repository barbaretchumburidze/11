package com.example.login
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.CheckBox
import android.widget.EditText
import android.widget.Toast
import java.util.jar.Attributes

class MainActivity : AppCompatActivity() {
    private lateinit var NameInput: EditText
    private lateinit var LastNameInput: EditText
    private lateinit var EmailInput: EditText
    private lateinit var PasswordInput: EditText
    private lateinit var RegistrationButton: Button
    private lateinit var boxInput: CheckBox

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        NameInput = findViewById(R.id.Name)
        LastNameInput = findViewById((R.id.LastName))
        EmailInput = findViewById(R.id.EmailAddress)
        PasswordInput = findViewById(R.id.Password)
        RegistrationButton = findViewById(R.id.button)

        RegistrationButton.setOnClickListener(){
            var name = NameInput.text.toString()
            var lastname = LastNameInput.text.toString()
            var Email = EmailInput.text.toString()
            var Password = PasswordInput.text.toString()
            var Box = boxInput

            if(name.isNotEmpty() or (name.length < 3)) {
                NameInput.error = "შეიყვანეთ სწორი სახელი"
                return@setOnClickListener}
            if(lastname.isNotEmpty() or (lastname.length < 5)) {
                LastNameInput.error = "შეიყვანეთ სწორი გვარი"
                return@setOnClickListener}
            if (!Email.contains("@")) {
                EmailInput.error = "შეიყვანეთ სწორი იმეილი"
                return@setOnClickListener }
            if(Password.isNotEmpty() && Password.length < 8) {
                PasswordInput.error = "პაროლში შეიყვანენ მინიმუმ 8 სიმბოლო"
                return@setOnClickListener }
            if(!Box.isChecked) {
                boxInput.error = "დაეთანხმეთ"
            return@setOnClickListener }

            if (name.isNotEmpty() && name.length > 3 && lastname.isNotEmpty() && lastname.length >5 && !Email.contains("@") && Password.isNotEmpty() && Password.length >8 && Box.isChecked) {
                Toast.makeText(this, "რეგისტრაცია წარმატებით დასრულდა", Toast.LENGTH_SHORT).show()
            }
        }
    }
}